TITLE
----
JSON API DOCUMENTATION
----

1. Get random node from db in JSON format
    URL /json_api/random
    METHODS GET | POST
    SUCCESS RESPONSE:
        code: 200
        content: {
                    "type":"Node_type",
                    "nid":"Node_ID",
                    "title":"Node_title",
                    "created":"Timestamp creation",
                    "text":"Node_body"
                  }
        watchdog: "JSON API request passed: ".<Query string>
    ERROR RESPONSE:
         ERROR TYPE - WRONG PATH:
            code: 404
            content: <error page>
         ERROR TYPE - EMPTY ARRAY:
            content: {
                        "empty array error": "JSON API request failed - no data there"
                      }
             watchdog: "JSON API request failed: ".<Query string>

2. Get random node of special type from db in JSON format
    URL /json_api/random/<node_type>
    METHODS GET | POST
    SUCCESS RESPONSE:
        code: 200
        content: {
                    "type":"Node_type",
                    "nid":"Node_ID",
                    "title":"Node_title",
                    "created":"Timestamp creation",
                    "text":"Node_body"
                  }
        watchdog: "JSON API request passed: ".<Query string>
    ERROR RESPONSE:
         ERROR TYPE - WRONG PATH | WRONG NODE_TYPE:
            code: 404
            content: <error page>
         ERROR TYPE - EMPTY ARRAY:
            content: {
                        "empty array error": "JSON API request failed - no data there"
                      }
             watchdog: "JSON API request failed: ".<Query string>

3. Get from DB node by node_id, converts it to JSON
    URL /json_api/specific_node/<node_id>
    METHODS GET | POST
    SUCCESS RESPONSE:
        code: 200
        content: {
                    "type":"Node_type",
                    "nid":"Node_ID",
                    "title":"Node_title",
                    "created":"Timestamp creation",
                    "text":"Node_body"
                  }
        watchdog: "JSON API request passed: ".<Query string>
    ERROR RESPONSE:
         ERROR TYPE - WRONG PATH | WRONG NODE_ID:
            code: 404
            content: <error page>
         ERROR TYPE - EMPTY ARRAY:
            content: {
                        "empty array error": "JSON API request failed - no data there"
                      }
             watchdog: "JSON API request failed: ".<Query string>

4. Get from DB nodes of some node_type from start_node_id to end_node_id, converts it to JSON
    URL /json_api/<node_type>/<start node_id>/<end node_id>
    METHODS GET | POST
    SUCCESS RESPONSE:
        code: 200
        content: {
                    "Node_ID":
                    {
                        "type":"Node_type",
                        "nid":"Node_ID",
                        "title":"Node_title",
                        "created":"Timestamp creation",
                        "text":"Node_body"
                    },
                    ...
                  }
        watchdog: "JSON API request passed: ".<Query string>
    ERROR RESPONSE:
         ERROR TYPE - WRONG PATH | WRONG NODE_TYPE | WRONG NODE_ID:
            code: 404
            content: <error page>
         ERROR TYPE - WRONG RANGE:
            content: {
                        'error range' => 'JSON API wrong node_id range'
                      }
             watchdog: "JSON API wrong node_id range"
         ERROR TYPE - EMPTY ARRAY:
            content: {
                        "empty array error": "JSON API request failed - no data there"
                      }
             watchdog: "JSON API request failed: ".<Query string>